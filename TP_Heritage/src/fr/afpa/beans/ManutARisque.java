package fr.afpa.beans;

public class ManutARisque extends Manutentionnaire implements IPrime_Danger {

	public ManutARisque(String nom, String prenom, int age, String dateEntree, double nbreHeure) {
		super(nom, prenom, age, dateEntree, nbreHeure);

	}

	@Override
	public double calculerSalaire() {

		return super.calculerSalaire() + PRIME_DANGER;
	}
}

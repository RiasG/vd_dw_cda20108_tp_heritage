package fr.afpa.beans;

public final class Representant extends Vendeur {

	public Representant(String nom, String prenom, int age, String dateEntree, double chiffreAffaire) {
		super(nom, prenom, age, dateEntree, chiffreAffaire);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculerSalaire() {
		return super.calculerSalaire() + SALAIRE_FIXE;
	}

	public String getNom() {
		return super.getNom();
	}
}

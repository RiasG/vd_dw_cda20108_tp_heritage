package fr.afpa.beans;

public class Manutentionnaire extends Employe {
	
	private double nbreHeure;


	public Manutentionnaire(String nom, String prenom, int age, String dateEntree, double nbreHeure) {
		super(nom, prenom, age, dateEntree);
		this.nbreHeure=nbreHeure;
	}
	
	public double getNbreHeure() {
		return nbreHeure;
	}


	public void setNbreHeure(double nbreHeure) {
		this.nbreHeure = nbreHeure;
	}
	

	@Override
	public double calculerSalaire() {
		
		return nbreHeure * 20;
	}

}

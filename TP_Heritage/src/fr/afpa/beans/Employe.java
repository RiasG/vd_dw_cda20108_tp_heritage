package fr.afpa.beans;

public abstract class Employe {
	private String nom;
	private String prenom;
	private int age;
	private String dateEntree;
	
	
	public Employe(String nom, String prenom, int age, String dateEntree) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.dateEntree = dateEntree;
	}

	public abstract double calculerSalaire();
	
	public String getNom() {
		return nom + " " + prenom + " age : " + age + " ans , ";
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getDateEntree() {
		return dateEntree;
	}
	public void setDateEntree(String dateEntree) {
		this.dateEntree = dateEntree;
	}

}

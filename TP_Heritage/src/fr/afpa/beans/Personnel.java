package fr.afpa.beans;

public final class Personnel {
	private Employe[] listEmploye;

	public Personnel(Employe[] listEmploye) {
		this.listEmploye = listEmploye;
	}

	public Personnel() {
		// tableau � 100000 employes
		listEmploye = new Employe[100000];
	}

	public Employe[] getListEmploye() {
		return listEmploye;
	}

	public void setListEmploye(Employe[] listEmploye) {
		this.listEmploye = listEmploye;
	}

	public void ajouterEmploye(Employe employe) {
		// Verification si l'employe n'est pas null
		if (employe != null) {
			for (int i = 0; i < listEmploye.length; i++) {
				// Attribuer l'employe � la premi�re case vide du tableau
				if (listEmploye[i] == null) {
					listEmploye[i] = employe;
					// nous voulons l'inscrire qu'une seule fois, il est donc n�cessaire de faire un
					// break;
					break;
				}
			}
		}
	}

	// Calcule tout les salaires et les affiches ave le nom, prenom, age de la personne
	public void afficherSalaires() {
		for (int i = 0; i < listEmploye.length; i++) {
			if (listEmploye[i] != null) {
				System.out.println(listEmploye[i].getNom() + " salaire : " + listEmploye[i].calculerSalaire() + " euros");
			}
		}
	}

	public double salaireMoyen() {
		double moyenne = 0;
		int nbEmployee = 0;
		for (int i = 0; i < listEmploye.length; i++) {
			if (listEmploye[i] != null) {
				nbEmployee++;
				moyenne += listEmploye[i].calculerSalaire();
			}
		}
		if(nbEmployee > 0) {
			moyenne =  moyenne / nbEmployee;
		}
		return moyenne;
		
	}

}

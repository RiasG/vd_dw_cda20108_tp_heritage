package fr.afpa.beans;

public class Technicien extends Employe {
	
	private double unitProd;
	
	public Technicien(String nom, String prenom, int age, String dateEntree, double unitProd) {
		super(nom, prenom, age, dateEntree);
		this.unitProd = unitProd;
	
	}

	public double getUnitProd() {
		return unitProd;
	}

	public void setUnitProd(double unitProd) {
		this.unitProd = unitProd;
	}

	@Override
	public double calculerSalaire() {
		
		return unitProd * 5;
	}
}

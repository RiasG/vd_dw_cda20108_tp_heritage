package fr.afpa.beans;

public class Vendeur extends Employe {
	// Le chiffre d'affaire est nécessaire pour le calcul du salaire du Vendeur et du Représentant
	private double chiffreAffaire;
	private final static double TAUX_CA = 0.20;
	protected final static int SALAIRE_FIXE = 400;
	private String profession;
	
	

	public Vendeur(String nom, String prenom, int age, String dateEntree, double chiffreAffaire ) {
		super(nom, prenom, age, dateEntree);
		this.chiffreAffaire = chiffreAffaire;
	}

	@Override
	public double calculerSalaire() {
		return SALAIRE_FIXE + (chiffreAffaire * TAUX_CA);
	}


	public double getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(double chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}
	
	@Override
	public String getNom() {
		return super.getNom();
	}

}

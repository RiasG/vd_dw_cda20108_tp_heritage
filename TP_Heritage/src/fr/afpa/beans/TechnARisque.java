package fr.afpa.beans;

public class TechnARisque extends Technicien implements IPrime_Danger {

	public TechnARisque(String nom, String prenom, int age, String dateEntree, double unitProd) {
		super(nom, prenom, age, dateEntree, unitProd);

	}

	@Override
	public double calculerSalaire() {

		return super.calculerSalaire() + PRIME_DANGER;
	}

}
